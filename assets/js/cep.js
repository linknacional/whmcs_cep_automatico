/*
    * Criado por Victor Hugo Scatolon de Souza 
    * Alterado por Davi Souza 13/06/2018
*/

$(document).ready(function() {
    // -- Configurações --
    var esconder_campos = true;
    var loading_gif = "/cliente/assets/img/loading.gif";

    $("#inputPostcode").after("<span id='searchCep' style='cursor:pointer;' class='field-help-text'>Clique aqui para preencher o endereço automáticamente pelo CEP</span>");

    
    $("#searchCep").click(function(event) {
        if ($('#inputPostcode').val() == ""){
            alert('Digite um CEP');
            $("#inputPostcode").focus();
        }else{
            $("#inputPostcode").focus().removeClass('is-invalid');
            $("#inputAddress1").removeClass('is-invalid');
            $("#inputAddress2").removeClass('is-invalid');
            $("#inputCity").removeClass('is-invalid');
            $("#stateselect").removeClass('is-invalid');

            var cepTratado = $("#inputPostcode").val().replace(/\D+/g, '');
            //cepTratado
            $("#inputPostcode").val(cepTratado);
            $("#inputPostcode").after("<p id='cep-loading'><img src='"+loading_gif+"' /></p>");

            $.get("//ddd.pricez.com.br/cep/"+cepTratado+".json", function(data) {
                //console.log(data);
                 if(data.payload.logradouro != null){
                    $("#inputAddress1").val(data.payload.logradouro+", 1");
                    //$("#inputPostcode").addClass('is-invalid');
                    $("#inputAddress1").after("<span class='field-help-text'>Insira o número do endereço.</span>");
                    $("#inputAddress1").focus();
                }
                if(data.payload.cidade != null){                
                    $("#inputAddress2").val(data.payload.bairro);
                    $("#inputCity").val(data.payload.cidade);
                    $("#stateselect").val(data.payload.estado);
                    $("#searchCep").remove();

                }else{
                    alert("Infelizmente, não encontramos nenhum endereço associado ao CEP digitado, por favor verifique o número. Se estiver correto, por gentileza preencha o endereço manualmente.");
                    $("#inputPostcode").focus().addClass('is-invalid');
                    $("#inputAddress1").addClass('is-invalid');
                    $("#inputAddress2").addClass('is-invalid');
                    $("#inputCity").addClass('is-invalid');
                    $("#stateselect").addClass('is-invalid');
                }
            }).fail(function() {
                alert("Ocorreu um erro interno ao buscar os dados do CEP. Tente novamente ou preencha manualmente.");
                $("#inputPostcode").focus();
            }).always(function() {
                $("#cep-loading").remove();
            });
        }
        
    });
});